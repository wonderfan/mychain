package keeper

import (
	"context"

	sdk "github.com/cosmos/cosmos-sdk/types"
	"github.com/zhigui-projects/zeus-chain/x/jvm/types"
)

func (k msgServer) MsgDeployContract(goCtx context.Context, msg *types.MsgMsgDeployContract) (*types.MsgMsgDeployContractResponse, error) {
	ctx := sdk.UnwrapSDKContext(goCtx)

	// TODO: Handling the message
	_ = ctx

	return &types.MsgMsgDeployContractResponse{}, nil
}
