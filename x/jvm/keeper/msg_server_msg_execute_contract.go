package keeper

import (
	"context"

	sdk "github.com/cosmos/cosmos-sdk/types"
	"github.com/zhigui-projects/zeus-chain/x/jvm/types"
)

func (k msgServer) MsgExecuteContract(goCtx context.Context, msg *types.MsgMsgExecuteContract) (*types.MsgMsgExecuteContractResponse, error) {
	ctx := sdk.UnwrapSDKContext(goCtx)

	// TODO: Handling the message
	_ = ctx

	return &types.MsgMsgExecuteContractResponse{}, nil
}
