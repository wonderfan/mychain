package keeper

import (
	"context"

	sdk "github.com/cosmos/cosmos-sdk/types"
	"github.com/zhigui-projects/zeus-chain/x/jvm/types"
)

func (k msgServer) MsgUpgradeContract(goCtx context.Context, msg *types.MsgMsgUpgradeContract) (*types.MsgMsgUpgradeContractResponse, error) {
	ctx := sdk.UnwrapSDKContext(goCtx)

	// TODO: Handling the message
	_ = ctx

	return &types.MsgMsgUpgradeContractResponse{}, nil
}
