rm package types

import (
	sdk "github.com/cosmos/cosmos-sdk/types"
	sdkerrors "github.com/cosmos/cosmos-sdk/types/errors"
)

var _ sdk.Msg = &MsgMsgUpgradeContract{}

func NewMsgMsgUpgradeContract(creator string, from string, id string, byteCode string) *MsgMsgUpgradeContract {
	return &MsgMsgUpgradeContract{
		Creator:  creator,
		From:     from,
		Id:       id,
		ByteCode: byteCode,
	}
}

func (msg *MsgMsgUpgradeContract) Route() string {
	return RouterKey
}

func (msg *MsgMsgUpgradeContract) Type() string {
	return "MsgUpgradeContract"
}

func (msg *MsgMsgUpgradeContract) GetSigners() []sdk.AccAddress {
	creator, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		panic(err)
	}
	return []sdk.AccAddress{creator}
}

func (msg *MsgMsgUpgradeContract) GetSignBytes() []byte {
	bz := ModuleCdc.MustMarshalJSON(msg)
	return sdk.MustSortJSON(bz)
}

func (msg *MsgMsgUpgradeContract) ValidateBasic() error {
	_, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		return sdkerrors.Wrapf(sdkerrors.ErrInvalidAddress, "invalid creator address (%s)", err)
	}
	return nil
}
