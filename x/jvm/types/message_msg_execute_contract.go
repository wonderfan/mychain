package types

import (
	sdk "github.com/cosmos/cosmos-sdk/types"
	sdkerrors "github.com/cosmos/cosmos-sdk/types/errors"
)

var _ sdk.Msg = &MsgMsgExecuteContract{}

func NewMsgMsgExecuteContract(creator string, from string, id string, method string, args string) *MsgMsgExecuteContract {
	return &MsgMsgExecuteContract{
		Creator: creator,
		From:    from,
		Id:      id,
		Method:  method,
		Args:    args,
	}
}

func (msg *MsgMsgExecuteContract) Route() string {
	return RouterKey
}

func (msg *MsgMsgExecuteContract) Type() string {
	return "MsgExecuteContract"
}

func (msg *MsgMsgExecuteContract) GetSigners() []sdk.AccAddress {
	creator, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		panic(err)
	}
	return []sdk.AccAddress{creator}
}

func (msg *MsgMsgExecuteContract) GetSignBytes() []byte {
	bz := ModuleCdc.MustMarshalJSON(msg)
	return sdk.MustSortJSON(bz)
}

func (msg *MsgMsgExecuteContract) ValidateBasic() error {
	_, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		return sdkerrors.Wrapf(sdkerrors.ErrInvalidAddress, "invalid creator address (%s)", err)
	}
	return nil
}
