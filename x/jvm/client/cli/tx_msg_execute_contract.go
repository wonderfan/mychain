package cli

import (
	"github.com/spf13/cobra"
	"strconv"

	"github.com/cosmos/cosmos-sdk/client"
	"github.com/cosmos/cosmos-sdk/client/flags"
	"github.com/cosmos/cosmos-sdk/client/tx"
	"github.com/zhigui-projects/zeus-chain/x/jvm/types"
)

var _ = strconv.Itoa(0)

func CmdMsgExecuteContract() *cobra.Command {
	cmd := &cobra.Command{
		Use:   "msg-execute-contract [from] [id] [method] [args]",
		Short: "Broadcast message MsgExecuteContract",
		Args:  cobra.ExactArgs(4),
		RunE: func(cmd *cobra.Command, args []string) error {
			argsFrom := string(args[0])
			argsId := string(args[1])
			argsMethod := string(args[2])
			argsArgs := string(args[3])

			clientCtx, err := client.GetClientTxContext(cmd)
			if err != nil {
				return err
			}

			msg := types.NewMsgMsgExecuteContract(clientCtx.GetFromAddress().String(), string(argsFrom), string(argsId), string(argsMethod), string(argsArgs))
			if err := msg.ValidateBasic(); err != nil {
				return err
			}
			return tx.GenerateOrBroadcastTxCLI(clientCtx, cmd.Flags(), msg)
		},
	}

	flags.AddTxFlagsToCmd(cmd)

	return cmd
}
