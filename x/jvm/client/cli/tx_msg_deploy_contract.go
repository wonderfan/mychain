package cli

import (
	"github.com/spf13/cobra"
	"strconv"

	"github.com/cosmos/cosmos-sdk/client"
	"github.com/cosmos/cosmos-sdk/client/flags"
	"github.com/cosmos/cosmos-sdk/client/tx"
	"github.com/zhigui-projects/zeus-chain/x/jvm/types"
)

var _ = strconv.Itoa(0)

func CmdMsgDeployContract() *cobra.Command {
	cmd := &cobra.Command{
		Use:   "msg-deploy-contract [from] [id] [byteCode]",
		Short: "Broadcast message MsgDeployContract",
		Args:  cobra.ExactArgs(3),
		RunE: func(cmd *cobra.Command, args []string) error {
			argsFrom := string(args[0])
			argsId := string(args[1])
			argsByteCode := string(args[2])

			clientCtx, err := client.GetClientTxContext(cmd)
			if err != nil {
				return err
			}

			msg := types.NewMsgMsgDeployContract(clientCtx.GetFromAddress().String(), string(argsFrom), string(argsId), string(argsByteCode))
			if err := msg.ValidateBasic(); err != nil {
				return err
			}
			return tx.GenerateOrBroadcastTxCLI(clientCtx, cmd.Flags(), msg)
		},
	}

	flags.AddTxFlagsToCmd(cmd)

	return cmd
}
