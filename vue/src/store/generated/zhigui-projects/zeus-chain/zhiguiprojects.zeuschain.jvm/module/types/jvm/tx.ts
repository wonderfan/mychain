/* eslint-disable */
import { Reader, Writer } from 'protobufjs/minimal'

export const protobufPackage = 'zhiguiprojects.zeuschain.jvm'

/** this line is used by starport scaffolding # proto/tx/message */
export interface MsgMsgUpgradeContract {
  creator: string
  from: string
  id: string
  byteCode: string
}

export interface MsgMsgUpgradeContractResponse {}

export interface MsgMsgExecuteContract {
  creator: string
  from: string
  id: string
  method: string
  args: string
}

export interface MsgMsgExecuteContractResponse {}

export interface MsgMsgDeployContract {
  creator: string
  from: string
  id: string
  byteCode: string
}

export interface MsgMsgDeployContractResponse {}

const baseMsgMsgUpgradeContract: object = { creator: '', from: '', id: '', byteCode: '' }

export const MsgMsgUpgradeContract = {
  encode(message: MsgMsgUpgradeContract, writer: Writer = Writer.create()): Writer {
    if (message.creator !== '') {
      writer.uint32(10).string(message.creator)
    }
    if (message.from !== '') {
      writer.uint32(18).string(message.from)
    }
    if (message.id !== '') {
      writer.uint32(26).string(message.id)
    }
    if (message.byteCode !== '') {
      writer.uint32(34).string(message.byteCode)
    }
    return writer
  },

  decode(input: Reader | Uint8Array, length?: number): MsgMsgUpgradeContract {
    const reader = input instanceof Uint8Array ? new Reader(input) : input
    let end = length === undefined ? reader.len : reader.pos + length
    const message = { ...baseMsgMsgUpgradeContract } as MsgMsgUpgradeContract
    while (reader.pos < end) {
      const tag = reader.uint32()
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string()
          break
        case 2:
          message.from = reader.string()
          break
        case 3:
          message.id = reader.string()
          break
        case 4:
          message.byteCode = reader.string()
          break
        default:
          reader.skipType(tag & 7)
          break
      }
    }
    return message
  },

  fromJSON(object: any): MsgMsgUpgradeContract {
    const message = { ...baseMsgMsgUpgradeContract } as MsgMsgUpgradeContract
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = String(object.creator)
    } else {
      message.creator = ''
    }
    if (object.from !== undefined && object.from !== null) {
      message.from = String(object.from)
    } else {
      message.from = ''
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = String(object.id)
    } else {
      message.id = ''
    }
    if (object.byteCode !== undefined && object.byteCode !== null) {
      message.byteCode = String(object.byteCode)
    } else {
      message.byteCode = ''
    }
    return message
  },

  toJSON(message: MsgMsgUpgradeContract): unknown {
    const obj: any = {}
    message.creator !== undefined && (obj.creator = message.creator)
    message.from !== undefined && (obj.from = message.from)
    message.id !== undefined && (obj.id = message.id)
    message.byteCode !== undefined && (obj.byteCode = message.byteCode)
    return obj
  },

  fromPartial(object: DeepPartial<MsgMsgUpgradeContract>): MsgMsgUpgradeContract {
    const message = { ...baseMsgMsgUpgradeContract } as MsgMsgUpgradeContract
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = object.creator
    } else {
      message.creator = ''
    }
    if (object.from !== undefined && object.from !== null) {
      message.from = object.from
    } else {
      message.from = ''
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = object.id
    } else {
      message.id = ''
    }
    if (object.byteCode !== undefined && object.byteCode !== null) {
      message.byteCode = object.byteCode
    } else {
      message.byteCode = ''
    }
    return message
  }
}

const baseMsgMsgUpgradeContractResponse: object = {}

export const MsgMsgUpgradeContractResponse = {
  encode(_: MsgMsgUpgradeContractResponse, writer: Writer = Writer.create()): Writer {
    return writer
  },

  decode(input: Reader | Uint8Array, length?: number): MsgMsgUpgradeContractResponse {
    const reader = input instanceof Uint8Array ? new Reader(input) : input
    let end = length === undefined ? reader.len : reader.pos + length
    const message = { ...baseMsgMsgUpgradeContractResponse } as MsgMsgUpgradeContractResponse
    while (reader.pos < end) {
      const tag = reader.uint32()
      switch (tag >>> 3) {
        default:
          reader.skipType(tag & 7)
          break
      }
    }
    return message
  },

  fromJSON(_: any): MsgMsgUpgradeContractResponse {
    const message = { ...baseMsgMsgUpgradeContractResponse } as MsgMsgUpgradeContractResponse
    return message
  },

  toJSON(_: MsgMsgUpgradeContractResponse): unknown {
    const obj: any = {}
    return obj
  },

  fromPartial(_: DeepPartial<MsgMsgUpgradeContractResponse>): MsgMsgUpgradeContractResponse {
    const message = { ...baseMsgMsgUpgradeContractResponse } as MsgMsgUpgradeContractResponse
    return message
  }
}

const baseMsgMsgExecuteContract: object = { creator: '', from: '', id: '', method: '', args: '' }

export const MsgMsgExecuteContract = {
  encode(message: MsgMsgExecuteContract, writer: Writer = Writer.create()): Writer {
    if (message.creator !== '') {
      writer.uint32(10).string(message.creator)
    }
    if (message.from !== '') {
      writer.uint32(18).string(message.from)
    }
    if (message.id !== '') {
      writer.uint32(26).string(message.id)
    }
    if (message.method !== '') {
      writer.uint32(34).string(message.method)
    }
    if (message.args !== '') {
      writer.uint32(42).string(message.args)
    }
    return writer
  },

  decode(input: Reader | Uint8Array, length?: number): MsgMsgExecuteContract {
    const reader = input instanceof Uint8Array ? new Reader(input) : input
    let end = length === undefined ? reader.len : reader.pos + length
    const message = { ...baseMsgMsgExecuteContract } as MsgMsgExecuteContract
    while (reader.pos < end) {
      const tag = reader.uint32()
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string()
          break
        case 2:
          message.from = reader.string()
          break
        case 3:
          message.id = reader.string()
          break
        case 4:
          message.method = reader.string()
          break
        case 5:
          message.args = reader.string()
          break
        default:
          reader.skipType(tag & 7)
          break
      }
    }
    return message
  },

  fromJSON(object: any): MsgMsgExecuteContract {
    const message = { ...baseMsgMsgExecuteContract } as MsgMsgExecuteContract
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = String(object.creator)
    } else {
      message.creator = ''
    }
    if (object.from !== undefined && object.from !== null) {
      message.from = String(object.from)
    } else {
      message.from = ''
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = String(object.id)
    } else {
      message.id = ''
    }
    if (object.method !== undefined && object.method !== null) {
      message.method = String(object.method)
    } else {
      message.method = ''
    }
    if (object.args !== undefined && object.args !== null) {
      message.args = String(object.args)
    } else {
      message.args = ''
    }
    return message
  },

  toJSON(message: MsgMsgExecuteContract): unknown {
    const obj: any = {}
    message.creator !== undefined && (obj.creator = message.creator)
    message.from !== undefined && (obj.from = message.from)
    message.id !== undefined && (obj.id = message.id)
    message.method !== undefined && (obj.method = message.method)
    message.args !== undefined && (obj.args = message.args)
    return obj
  },

  fromPartial(object: DeepPartial<MsgMsgExecuteContract>): MsgMsgExecuteContract {
    const message = { ...baseMsgMsgExecuteContract } as MsgMsgExecuteContract
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = object.creator
    } else {
      message.creator = ''
    }
    if (object.from !== undefined && object.from !== null) {
      message.from = object.from
    } else {
      message.from = ''
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = object.id
    } else {
      message.id = ''
    }
    if (object.method !== undefined && object.method !== null) {
      message.method = object.method
    } else {
      message.method = ''
    }
    if (object.args !== undefined && object.args !== null) {
      message.args = object.args
    } else {
      message.args = ''
    }
    return message
  }
}

const baseMsgMsgExecuteContractResponse: object = {}

export const MsgMsgExecuteContractResponse = {
  encode(_: MsgMsgExecuteContractResponse, writer: Writer = Writer.create()): Writer {
    return writer
  },

  decode(input: Reader | Uint8Array, length?: number): MsgMsgExecuteContractResponse {
    const reader = input instanceof Uint8Array ? new Reader(input) : input
    let end = length === undefined ? reader.len : reader.pos + length
    const message = { ...baseMsgMsgExecuteContractResponse } as MsgMsgExecuteContractResponse
    while (reader.pos < end) {
      const tag = reader.uint32()
      switch (tag >>> 3) {
        default:
          reader.skipType(tag & 7)
          break
      }
    }
    return message
  },

  fromJSON(_: any): MsgMsgExecuteContractResponse {
    const message = { ...baseMsgMsgExecuteContractResponse } as MsgMsgExecuteContractResponse
    return message
  },

  toJSON(_: MsgMsgExecuteContractResponse): unknown {
    const obj: any = {}
    return obj
  },

  fromPartial(_: DeepPartial<MsgMsgExecuteContractResponse>): MsgMsgExecuteContractResponse {
    const message = { ...baseMsgMsgExecuteContractResponse } as MsgMsgExecuteContractResponse
    return message
  }
}

const baseMsgMsgDeployContract: object = { creator: '', from: '', id: '', byteCode: '' }

export const MsgMsgDeployContract = {
  encode(message: MsgMsgDeployContract, writer: Writer = Writer.create()): Writer {
    if (message.creator !== '') {
      writer.uint32(10).string(message.creator)
    }
    if (message.from !== '') {
      writer.uint32(18).string(message.from)
    }
    if (message.id !== '') {
      writer.uint32(26).string(message.id)
    }
    if (message.byteCode !== '') {
      writer.uint32(34).string(message.byteCode)
    }
    return writer
  },

  decode(input: Reader | Uint8Array, length?: number): MsgMsgDeployContract {
    const reader = input instanceof Uint8Array ? new Reader(input) : input
    let end = length === undefined ? reader.len : reader.pos + length
    const message = { ...baseMsgMsgDeployContract } as MsgMsgDeployContract
    while (reader.pos < end) {
      const tag = reader.uint32()
      switch (tag >>> 3) {
        case 1:
          message.creator = reader.string()
          break
        case 2:
          message.from = reader.string()
          break
        case 3:
          message.id = reader.string()
          break
        case 4:
          message.byteCode = reader.string()
          break
        default:
          reader.skipType(tag & 7)
          break
      }
    }
    return message
  },

  fromJSON(object: any): MsgMsgDeployContract {
    const message = { ...baseMsgMsgDeployContract } as MsgMsgDeployContract
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = String(object.creator)
    } else {
      message.creator = ''
    }
    if (object.from !== undefined && object.from !== null) {
      message.from = String(object.from)
    } else {
      message.from = ''
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = String(object.id)
    } else {
      message.id = ''
    }
    if (object.byteCode !== undefined && object.byteCode !== null) {
      message.byteCode = String(object.byteCode)
    } else {
      message.byteCode = ''
    }
    return message
  },

  toJSON(message: MsgMsgDeployContract): unknown {
    const obj: any = {}
    message.creator !== undefined && (obj.creator = message.creator)
    message.from !== undefined && (obj.from = message.from)
    message.id !== undefined && (obj.id = message.id)
    message.byteCode !== undefined && (obj.byteCode = message.byteCode)
    return obj
  },

  fromPartial(object: DeepPartial<MsgMsgDeployContract>): MsgMsgDeployContract {
    const message = { ...baseMsgMsgDeployContract } as MsgMsgDeployContract
    if (object.creator !== undefined && object.creator !== null) {
      message.creator = object.creator
    } else {
      message.creator = ''
    }
    if (object.from !== undefined && object.from !== null) {
      message.from = object.from
    } else {
      message.from = ''
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = object.id
    } else {
      message.id = ''
    }
    if (object.byteCode !== undefined && object.byteCode !== null) {
      message.byteCode = object.byteCode
    } else {
      message.byteCode = ''
    }
    return message
  }
}

const baseMsgMsgDeployContractResponse: object = {}

export const MsgMsgDeployContractResponse = {
  encode(_: MsgMsgDeployContractResponse, writer: Writer = Writer.create()): Writer {
    return writer
  },

  decode(input: Reader | Uint8Array, length?: number): MsgMsgDeployContractResponse {
    const reader = input instanceof Uint8Array ? new Reader(input) : input
    let end = length === undefined ? reader.len : reader.pos + length
    const message = { ...baseMsgMsgDeployContractResponse } as MsgMsgDeployContractResponse
    while (reader.pos < end) {
      const tag = reader.uint32()
      switch (tag >>> 3) {
        default:
          reader.skipType(tag & 7)
          break
      }
    }
    return message
  },

  fromJSON(_: any): MsgMsgDeployContractResponse {
    const message = { ...baseMsgMsgDeployContractResponse } as MsgMsgDeployContractResponse
    return message
  },

  toJSON(_: MsgMsgDeployContractResponse): unknown {
    const obj: any = {}
    return obj
  },

  fromPartial(_: DeepPartial<MsgMsgDeployContractResponse>): MsgMsgDeployContractResponse {
    const message = { ...baseMsgMsgDeployContractResponse } as MsgMsgDeployContractResponse
    return message
  }
}

/** Msg defines the Msg service. */
export interface Msg {
  /** this line is used by starport scaffolding # proto/tx/rpc */
  MsgUpgradeContract(request: MsgMsgUpgradeContract): Promise<MsgMsgUpgradeContractResponse>
  MsgExecuteContract(request: MsgMsgExecuteContract): Promise<MsgMsgExecuteContractResponse>
  MsgDeployContract(request: MsgMsgDeployContract): Promise<MsgMsgDeployContractResponse>
}

export class MsgClientImpl implements Msg {
  private readonly rpc: Rpc
  constructor(rpc: Rpc) {
    this.rpc = rpc
  }
  MsgUpgradeContract(request: MsgMsgUpgradeContract): Promise<MsgMsgUpgradeContractResponse> {
    const data = MsgMsgUpgradeContract.encode(request).finish()
    const promise = this.rpc.request('zhiguiprojects.zeuschain.jvm.Msg', 'MsgUpgradeContract', data)
    return promise.then((data) => MsgMsgUpgradeContractResponse.decode(new Reader(data)))
  }

  MsgExecuteContract(request: MsgMsgExecuteContract): Promise<MsgMsgExecuteContractResponse> {
    const data = MsgMsgExecuteContract.encode(request).finish()
    const promise = this.rpc.request('zhiguiprojects.zeuschain.jvm.Msg', 'MsgExecuteContract', data)
    return promise.then((data) => MsgMsgExecuteContractResponse.decode(new Reader(data)))
  }

  MsgDeployContract(request: MsgMsgDeployContract): Promise<MsgMsgDeployContractResponse> {
    const data = MsgMsgDeployContract.encode(request).finish()
    const promise = this.rpc.request('zhiguiprojects.zeuschain.jvm.Msg', 'MsgDeployContract', data)
    return promise.then((data) => MsgMsgDeployContractResponse.decode(new Reader(data)))
  }
}

interface Rpc {
  request(service: string, method: string, data: Uint8Array): Promise<Uint8Array>
}

type Builtin = Date | Function | Uint8Array | string | number | undefined
export type DeepPartial<T> = T extends Builtin
  ? T
  : T extends Array<infer U>
  ? Array<DeepPartial<U>>
  : T extends ReadonlyArray<infer U>
  ? ReadonlyArray<DeepPartial<U>>
  : T extends {}
  ? { [K in keyof T]?: DeepPartial<T[K]> }
  : Partial<T>
