import { StdFee } from "@cosmjs/launchpad";
import { OfflineSigner, EncodeObject } from "@cosmjs/proto-signing";
import { Api } from "./rest";
import { MsgMsgUpgradeContract } from "./types/jvm/tx";
import { MsgMsgExecuteContract } from "./types/jvm/tx";
import { MsgMsgDeployContract } from "./types/jvm/tx";
export declare const MissingWalletError: Error;
interface TxClientOptions {
    addr: string;
}
interface SignAndBroadcastOptions {
    fee: StdFee;
    memo?: string;
}
declare const txClient: (wallet: OfflineSigner, { addr: addr }?: TxClientOptions) => Promise<{
    signAndBroadcast: (msgs: EncodeObject[], { fee, memo }?: SignAndBroadcastOptions) => Promise<import("@cosmjs/stargate").BroadcastTxResponse>;
    msgMsgUpgradeContract: (data: MsgMsgUpgradeContract) => EncodeObject;
    msgMsgExecuteContract: (data: MsgMsgExecuteContract) => EncodeObject;
    msgMsgDeployContract: (data: MsgMsgDeployContract) => EncodeObject;
}>;
interface QueryClientOptions {
    addr: string;
}
declare const queryClient: ({ addr: addr }?: QueryClientOptions) => Promise<Api<unknown>>;
export { txClient, queryClient, };
