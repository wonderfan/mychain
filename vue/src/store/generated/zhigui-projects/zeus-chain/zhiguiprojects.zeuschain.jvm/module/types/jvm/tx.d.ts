import { Reader, Writer } from 'protobufjs/minimal';
export declare const protobufPackage = "zhiguiprojects.zeuschain.jvm";
/** this line is used by starport scaffolding # proto/tx/message */
export interface MsgMsgUpgradeContract {
    creator: string;
    from: string;
    id: string;
    byteCode: string;
}
export interface MsgMsgUpgradeContractResponse {
}
export interface MsgMsgExecuteContract {
    creator: string;
    from: string;
    id: string;
    method: string;
    args: string;
}
export interface MsgMsgExecuteContractResponse {
}
export interface MsgMsgDeployContract {
    creator: string;
    from: string;
    id: string;
    byteCode: string;
}
export interface MsgMsgDeployContractResponse {
}
export declare const MsgMsgUpgradeContract: {
    encode(message: MsgMsgUpgradeContract, writer?: Writer): Writer;
    decode(input: Reader | Uint8Array, length?: number): MsgMsgUpgradeContract;
    fromJSON(object: any): MsgMsgUpgradeContract;
    toJSON(message: MsgMsgUpgradeContract): unknown;
    fromPartial(object: DeepPartial<MsgMsgUpgradeContract>): MsgMsgUpgradeContract;
};
export declare const MsgMsgUpgradeContractResponse: {
    encode(_: MsgMsgUpgradeContractResponse, writer?: Writer): Writer;
    decode(input: Reader | Uint8Array, length?: number): MsgMsgUpgradeContractResponse;
    fromJSON(_: any): MsgMsgUpgradeContractResponse;
    toJSON(_: MsgMsgUpgradeContractResponse): unknown;
    fromPartial(_: DeepPartial<MsgMsgUpgradeContractResponse>): MsgMsgUpgradeContractResponse;
};
export declare const MsgMsgExecuteContract: {
    encode(message: MsgMsgExecuteContract, writer?: Writer): Writer;
    decode(input: Reader | Uint8Array, length?: number): MsgMsgExecuteContract;
    fromJSON(object: any): MsgMsgExecuteContract;
    toJSON(message: MsgMsgExecuteContract): unknown;
    fromPartial(object: DeepPartial<MsgMsgExecuteContract>): MsgMsgExecuteContract;
};
export declare const MsgMsgExecuteContractResponse: {
    encode(_: MsgMsgExecuteContractResponse, writer?: Writer): Writer;
    decode(input: Reader | Uint8Array, length?: number): MsgMsgExecuteContractResponse;
    fromJSON(_: any): MsgMsgExecuteContractResponse;
    toJSON(_: MsgMsgExecuteContractResponse): unknown;
    fromPartial(_: DeepPartial<MsgMsgExecuteContractResponse>): MsgMsgExecuteContractResponse;
};
export declare const MsgMsgDeployContract: {
    encode(message: MsgMsgDeployContract, writer?: Writer): Writer;
    decode(input: Reader | Uint8Array, length?: number): MsgMsgDeployContract;
    fromJSON(object: any): MsgMsgDeployContract;
    toJSON(message: MsgMsgDeployContract): unknown;
    fromPartial(object: DeepPartial<MsgMsgDeployContract>): MsgMsgDeployContract;
};
export declare const MsgMsgDeployContractResponse: {
    encode(_: MsgMsgDeployContractResponse, writer?: Writer): Writer;
    decode(input: Reader | Uint8Array, length?: number): MsgMsgDeployContractResponse;
    fromJSON(_: any): MsgMsgDeployContractResponse;
    toJSON(_: MsgMsgDeployContractResponse): unknown;
    fromPartial(_: DeepPartial<MsgMsgDeployContractResponse>): MsgMsgDeployContractResponse;
};
/** Msg defines the Msg service. */
export interface Msg {
    /** this line is used by starport scaffolding # proto/tx/rpc */
    MsgUpgradeContract(request: MsgMsgUpgradeContract): Promise<MsgMsgUpgradeContractResponse>;
    MsgExecuteContract(request: MsgMsgExecuteContract): Promise<MsgMsgExecuteContractResponse>;
    MsgDeployContract(request: MsgMsgDeployContract): Promise<MsgMsgDeployContractResponse>;
}
export declare class MsgClientImpl implements Msg {
    private readonly rpc;
    constructor(rpc: Rpc);
    MsgUpgradeContract(request: MsgMsgUpgradeContract): Promise<MsgMsgUpgradeContractResponse>;
    MsgExecuteContract(request: MsgMsgExecuteContract): Promise<MsgMsgExecuteContractResponse>;
    MsgDeployContract(request: MsgMsgDeployContract): Promise<MsgMsgDeployContractResponse>;
}
interface Rpc {
    request(service: string, method: string, data: Uint8Array): Promise<Uint8Array>;
}
declare type Builtin = Date | Function | Uint8Array | string | number | undefined;
export declare type DeepPartial<T> = T extends Builtin ? T : T extends Array<infer U> ? Array<DeepPartial<U>> : T extends ReadonlyArray<infer U> ? ReadonlyArray<DeepPartial<U>> : T extends {} ? {
    [K in keyof T]?: DeepPartial<T[K]>;
} : Partial<T>;
export {};
