/* eslint-disable */
import { Reader, Writer } from 'protobufjs/minimal';
export const protobufPackage = 'zhiguiprojects.zeuschain.jvm';
const baseMsgMsgUpgradeContract = { creator: '', from: '', id: '', byteCode: '' };
export const MsgMsgUpgradeContract = {
    encode(message, writer = Writer.create()) {
        if (message.creator !== '') {
            writer.uint32(10).string(message.creator);
        }
        if (message.from !== '') {
            writer.uint32(18).string(message.from);
        }
        if (message.id !== '') {
            writer.uint32(26).string(message.id);
        }
        if (message.byteCode !== '') {
            writer.uint32(34).string(message.byteCode);
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof Uint8Array ? new Reader(input) : input;
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = { ...baseMsgMsgUpgradeContract };
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    message.creator = reader.string();
                    break;
                case 2:
                    message.from = reader.string();
                    break;
                case 3:
                    message.id = reader.string();
                    break;
                case 4:
                    message.byteCode = reader.string();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON(object) {
        const message = { ...baseMsgMsgUpgradeContract };
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = String(object.creator);
        }
        else {
            message.creator = '';
        }
        if (object.from !== undefined && object.from !== null) {
            message.from = String(object.from);
        }
        else {
            message.from = '';
        }
        if (object.id !== undefined && object.id !== null) {
            message.id = String(object.id);
        }
        else {
            message.id = '';
        }
        if (object.byteCode !== undefined && object.byteCode !== null) {
            message.byteCode = String(object.byteCode);
        }
        else {
            message.byteCode = '';
        }
        return message;
    },
    toJSON(message) {
        const obj = {};
        message.creator !== undefined && (obj.creator = message.creator);
        message.from !== undefined && (obj.from = message.from);
        message.id !== undefined && (obj.id = message.id);
        message.byteCode !== undefined && (obj.byteCode = message.byteCode);
        return obj;
    },
    fromPartial(object) {
        const message = { ...baseMsgMsgUpgradeContract };
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = object.creator;
        }
        else {
            message.creator = '';
        }
        if (object.from !== undefined && object.from !== null) {
            message.from = object.from;
        }
        else {
            message.from = '';
        }
        if (object.id !== undefined && object.id !== null) {
            message.id = object.id;
        }
        else {
            message.id = '';
        }
        if (object.byteCode !== undefined && object.byteCode !== null) {
            message.byteCode = object.byteCode;
        }
        else {
            message.byteCode = '';
        }
        return message;
    }
};
const baseMsgMsgUpgradeContractResponse = {};
export const MsgMsgUpgradeContractResponse = {
    encode(_, writer = Writer.create()) {
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof Uint8Array ? new Reader(input) : input;
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = { ...baseMsgMsgUpgradeContractResponse };
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON(_) {
        const message = { ...baseMsgMsgUpgradeContractResponse };
        return message;
    },
    toJSON(_) {
        const obj = {};
        return obj;
    },
    fromPartial(_) {
        const message = { ...baseMsgMsgUpgradeContractResponse };
        return message;
    }
};
const baseMsgMsgExecuteContract = { creator: '', from: '', id: '', method: '', args: '' };
export const MsgMsgExecuteContract = {
    encode(message, writer = Writer.create()) {
        if (message.creator !== '') {
            writer.uint32(10).string(message.creator);
        }
        if (message.from !== '') {
            writer.uint32(18).string(message.from);
        }
        if (message.id !== '') {
            writer.uint32(26).string(message.id);
        }
        if (message.method !== '') {
            writer.uint32(34).string(message.method);
        }
        if (message.args !== '') {
            writer.uint32(42).string(message.args);
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof Uint8Array ? new Reader(input) : input;
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = { ...baseMsgMsgExecuteContract };
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    message.creator = reader.string();
                    break;
                case 2:
                    message.from = reader.string();
                    break;
                case 3:
                    message.id = reader.string();
                    break;
                case 4:
                    message.method = reader.string();
                    break;
                case 5:
                    message.args = reader.string();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON(object) {
        const message = { ...baseMsgMsgExecuteContract };
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = String(object.creator);
        }
        else {
            message.creator = '';
        }
        if (object.from !== undefined && object.from !== null) {
            message.from = String(object.from);
        }
        else {
            message.from = '';
        }
        if (object.id !== undefined && object.id !== null) {
            message.id = String(object.id);
        }
        else {
            message.id = '';
        }
        if (object.method !== undefined && object.method !== null) {
            message.method = String(object.method);
        }
        else {
            message.method = '';
        }
        if (object.args !== undefined && object.args !== null) {
            message.args = String(object.args);
        }
        else {
            message.args = '';
        }
        return message;
    },
    toJSON(message) {
        const obj = {};
        message.creator !== undefined && (obj.creator = message.creator);
        message.from !== undefined && (obj.from = message.from);
        message.id !== undefined && (obj.id = message.id);
        message.method !== undefined && (obj.method = message.method);
        message.args !== undefined && (obj.args = message.args);
        return obj;
    },
    fromPartial(object) {
        const message = { ...baseMsgMsgExecuteContract };
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = object.creator;
        }
        else {
            message.creator = '';
        }
        if (object.from !== undefined && object.from !== null) {
            message.from = object.from;
        }
        else {
            message.from = '';
        }
        if (object.id !== undefined && object.id !== null) {
            message.id = object.id;
        }
        else {
            message.id = '';
        }
        if (object.method !== undefined && object.method !== null) {
            message.method = object.method;
        }
        else {
            message.method = '';
        }
        if (object.args !== undefined && object.args !== null) {
            message.args = object.args;
        }
        else {
            message.args = '';
        }
        return message;
    }
};
const baseMsgMsgExecuteContractResponse = {};
export const MsgMsgExecuteContractResponse = {
    encode(_, writer = Writer.create()) {
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof Uint8Array ? new Reader(input) : input;
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = { ...baseMsgMsgExecuteContractResponse };
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON(_) {
        const message = { ...baseMsgMsgExecuteContractResponse };
        return message;
    },
    toJSON(_) {
        const obj = {};
        return obj;
    },
    fromPartial(_) {
        const message = { ...baseMsgMsgExecuteContractResponse };
        return message;
    }
};
const baseMsgMsgDeployContract = { creator: '', from: '', id: '', byteCode: '' };
export const MsgMsgDeployContract = {
    encode(message, writer = Writer.create()) {
        if (message.creator !== '') {
            writer.uint32(10).string(message.creator);
        }
        if (message.from !== '') {
            writer.uint32(18).string(message.from);
        }
        if (message.id !== '') {
            writer.uint32(26).string(message.id);
        }
        if (message.byteCode !== '') {
            writer.uint32(34).string(message.byteCode);
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof Uint8Array ? new Reader(input) : input;
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = { ...baseMsgMsgDeployContract };
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    message.creator = reader.string();
                    break;
                case 2:
                    message.from = reader.string();
                    break;
                case 3:
                    message.id = reader.string();
                    break;
                case 4:
                    message.byteCode = reader.string();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON(object) {
        const message = { ...baseMsgMsgDeployContract };
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = String(object.creator);
        }
        else {
            message.creator = '';
        }
        if (object.from !== undefined && object.from !== null) {
            message.from = String(object.from);
        }
        else {
            message.from = '';
        }
        if (object.id !== undefined && object.id !== null) {
            message.id = String(object.id);
        }
        else {
            message.id = '';
        }
        if (object.byteCode !== undefined && object.byteCode !== null) {
            message.byteCode = String(object.byteCode);
        }
        else {
            message.byteCode = '';
        }
        return message;
    },
    toJSON(message) {
        const obj = {};
        message.creator !== undefined && (obj.creator = message.creator);
        message.from !== undefined && (obj.from = message.from);
        message.id !== undefined && (obj.id = message.id);
        message.byteCode !== undefined && (obj.byteCode = message.byteCode);
        return obj;
    },
    fromPartial(object) {
        const message = { ...baseMsgMsgDeployContract };
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = object.creator;
        }
        else {
            message.creator = '';
        }
        if (object.from !== undefined && object.from !== null) {
            message.from = object.from;
        }
        else {
            message.from = '';
        }
        if (object.id !== undefined && object.id !== null) {
            message.id = object.id;
        }
        else {
            message.id = '';
        }
        if (object.byteCode !== undefined && object.byteCode !== null) {
            message.byteCode = object.byteCode;
        }
        else {
            message.byteCode = '';
        }
        return message;
    }
};
const baseMsgMsgDeployContractResponse = {};
export const MsgMsgDeployContractResponse = {
    encode(_, writer = Writer.create()) {
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof Uint8Array ? new Reader(input) : input;
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = { ...baseMsgMsgDeployContractResponse };
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON(_) {
        const message = { ...baseMsgMsgDeployContractResponse };
        return message;
    },
    toJSON(_) {
        const obj = {};
        return obj;
    },
    fromPartial(_) {
        const message = { ...baseMsgMsgDeployContractResponse };
        return message;
    }
};
export class MsgClientImpl {
    constructor(rpc) {
        this.rpc = rpc;
    }
    MsgUpgradeContract(request) {
        const data = MsgMsgUpgradeContract.encode(request).finish();
        const promise = this.rpc.request('zhiguiprojects.zeuschain.jvm.Msg', 'MsgUpgradeContract', data);
        return promise.then((data) => MsgMsgUpgradeContractResponse.decode(new Reader(data)));
    }
    MsgExecuteContract(request) {
        const data = MsgMsgExecuteContract.encode(request).finish();
        const promise = this.rpc.request('zhiguiprojects.zeuschain.jvm.Msg', 'MsgExecuteContract', data);
        return promise.then((data) => MsgMsgExecuteContractResponse.decode(new Reader(data)));
    }
    MsgDeployContract(request) {
        const data = MsgMsgDeployContract.encode(request).finish();
        const promise = this.rpc.request('zhiguiprojects.zeuschain.jvm.Msg', 'MsgDeployContract', data);
        return promise.then((data) => MsgMsgDeployContractResponse.decode(new Reader(data)));
    }
}
