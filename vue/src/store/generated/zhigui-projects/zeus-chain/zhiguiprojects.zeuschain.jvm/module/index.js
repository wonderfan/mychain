// THIS FILE IS GENERATED AUTOMATICALLY. DO NOT MODIFY.
import { SigningStargateClient } from "@cosmjs/stargate";
import { Registry } from "@cosmjs/proto-signing";
import { Api } from "./rest";
import { MsgMsgUpgradeContract } from "./types/jvm/tx";
import { MsgMsgExecuteContract } from "./types/jvm/tx";
import { MsgMsgDeployContract } from "./types/jvm/tx";
const types = [
    ["/zhiguiprojects.zeuschain.jvm.MsgMsgUpgradeContract", MsgMsgUpgradeContract],
    ["/zhiguiprojects.zeuschain.jvm.MsgMsgExecuteContract", MsgMsgExecuteContract],
    ["/zhiguiprojects.zeuschain.jvm.MsgMsgDeployContract", MsgMsgDeployContract],
];
export const MissingWalletError = new Error("wallet is required");
const registry = new Registry(types);
const defaultFee = {
    amount: [],
    gas: "200000",
};
const txClient = async (wallet, { addr: addr } = { addr: "http://localhost:26657" }) => {
    if (!wallet)
        throw MissingWalletError;
    const client = await SigningStargateClient.connectWithSigner(addr, wallet, { registry });
    const { address } = (await wallet.getAccounts())[0];
    return {
        signAndBroadcast: (msgs, { fee, memo } = { fee: defaultFee, memo: "" }) => client.signAndBroadcast(address, msgs, fee, memo),
        msgMsgUpgradeContract: (data) => ({ typeUrl: "/zhiguiprojects.zeuschain.jvm.MsgMsgUpgradeContract", value: data }),
        msgMsgExecuteContract: (data) => ({ typeUrl: "/zhiguiprojects.zeuschain.jvm.MsgMsgExecuteContract", value: data }),
        msgMsgDeployContract: (data) => ({ typeUrl: "/zhiguiprojects.zeuschain.jvm.MsgMsgDeployContract", value: data }),
    };
};
const queryClient = async ({ addr: addr } = { addr: "http://localhost:1317" }) => {
    return new Api({ baseUrl: addr });
};
export { txClient, queryClient, };
